'use strict'

const response = require('../exchange/response')

exports.canCreate = (req, res, next) => {

    if (!req.body.name) {
        response.failure(res, 'name is required')
    }
    if (!req.body.image) {
        response.failure(res, 'image is required')
    } 
    
    return next()
}

exports.getById = (req, res, next) => {

    if (!req.params && !req.params.id) {

        return response.failure(res, 'id is required')
    }

    return next()
}

exports.update = (req, res, next) => {
    if (!req.params && !req.params.id) {
        return response.failure(res, 'id is required')

    }
    return next()
}

exports.delete = (req, res, next) => {
    if (!req.params && !req.params.id) {
        return response.failure(res, 'id is required')

    }
    return next()
}

// exports.login = (req, res, next) => {
//     if (!req.body.email && !req.body.phone) {
//         response.failure(res, 'Phone number is required')
//     }
//     if (!req.body.password) {
//         response.failure(res, 'Password  is required')
//     }
//     return next()
// }