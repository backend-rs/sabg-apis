'use strict'

const response = require('../exchange/response')

exports.create = (req, res, next) => {

    if (!req.body.name) {
        response.failure(res, 'name is required')
    }
    if (!req.body.image) {
        response.failure(res, 'image url is required')
    } 
    if(!req.body.maketPrice){
        responce.failure(res,'maket price is required')
        
    }
    if(!req.body.price){
        responce.failure(res,'price is required')
        
    }
    return next()
}

exports.getById = (req, res, next) => {

    if (!req.params && !req.params.id) {

        return response.failure(res, 'id is required')
    }

    return next()
}

exports.update = (req, res, next) => {
    if (!req.params && !req.params.id) {
        return response.failure(res, 'id is required')

    }
    return next()
}