"use strict";
const encrypt = require("../permit/crypto");
const jwt = require("jsonwebtoken");
const authConfig = require("config").get("auth");
const auth = require("../permit/auth");
const service = require("./carts");
// const userService = require('./users')

const randomize = require("randomatic");
const moment = require("moment");
const https = require("axios");
const response = require("../exchange/response");
const nodemailer = require("nodemailer");

const apikey = "u80rypfvfJc-dLn4G9htETrHLToVF9ziofaSGiU0Jc";
const url = "https://api.textlocal.in/send";
const sender = "SABJEE";

const sendMail = async (email, transporter, subject, text, html) => {
  const details = {
    from: "infosabg0@gmail.com",
    // to: email, // Receiver's email id
    bcc: email,
    subject: subject,
    text: text,
    html: html,
  };

  var info = await transporter.sendMail(details);
  console.log("INFO:::", info);
};
const set = (model, entity, context) => {
  const log = context.logger.start("services/users/set");
  try {
    if (model.image) {
      entity.profile.image = model.image;
    }

    if (model.firstName) {
      entity.profile.firstName = model.firstName;
    }
    if (model.lastName) {
      entity.profile.lastName = model.lastName;
    }
    if (model.userName) {
      entity.userName = model.userName;
    }
    if (model.phone) {
      entity.phone = model.phone;
    }
    if (model.email) {
      entity.profile.email = model.email;
    }
    if (model.address && model.address.city) {
      entity.profile.address.city = model.address.city;
    }
    if (model.address && model.address.addressLine) {
      entity.profile.address.addressLine = model.address.addressLine;
    }
    if (model.address && model.address.apartmentName) {
      entity.profile.address.apartmentName = model.address.apartmentName;
    }
    if (model.address && model.address.floor) {
      entity.profile.address.floor = model.address.floor;
    }
    if (model.deliveryAddress) {
      if (model.deliveryAddress && model.deliveryAddress.firstName) {
        entity.profile.deliveryAddress.firstName =
          model.deliveryAddress.firstName;
      }
      if (model.deliveryAddress && model.deliveryAddress.lastName) {
        entity.profile.deliveryAddress.lastName =
          model.deliveryAddress.lastName;
      }
      if (model.deliveryAddress && model.deliveryAddress.contactNumber) {
        entity.profile.deliveryAddress.contactNumber =
          model.deliveryAddress.contactNumber;
      }
      if (model.deliveryAddress && model.deliveryAddress.city) {
        entity.profile.deliveryAddress.city = model.deliveryAddress.city;
      }
      if (model.deliveryAddress && model.deliveryAddress.addressLine) {
        entity.profile.deliveryAddress.addressLine =
          model.deliveryAddress.addressLine;
      }
      if (model.deliveryAddress && model.deliveryAddress.apartmentName) {
        entity.profile.deliveryAddress.apartmentName =
          model.deliveryAddress.apartmentName;
      }
      if (model.deliveryAddress && model.deliveryAddress.floor) {
        entity.profile.deliveryAddress.floor = model.deliveryAddress.floor;
      }
    } else {
      entity.profile.deliveryAddress.firstName = model.firstName;
      entity.profile.deliveryAddress.lastName = model.lastName;
      entity.profile.deliveryAddress.contactNumber = model.contactNumber;
      if (model.address) {
        entity.profile.deliveryAddress.city = model.address.city;
        entity.profile.deliveryAddress.apartmentName =
          model.address.apartmentName;
        entity.profile.deliveryAddress.floor = model.address.floor;
        entity.profile.deliveryAddress.addressLine = model.address.addressLine;
      }
    }

    log.end();
    return entity;
  } catch (err) {
    throw new Error(err);
  }
};

// send sms method
const sendSms = async (url, apikey, sender, subject, otp, phone) => {
  let data;
  data =
    "apikey=" +
    apikey +
    "&sender=" +
    sender +
    "&numbers=" +
    phone +
    "&message=" +
    subject +
    otp;
  //  data='apikey=' + apikey;
  https
    .post(`https://api.textlocal.in/send?${data}`, {})
    .then((response) => {
      //  https.get(`https://api.textlocal.in/get_sender_names/?${data}`, {}).then(response => {

      console.log(response);
    })
    .catch((error) => {
      console.log(error);
    });
};

// sendOtp method
const sendOtp = async (model, user, context) => {
  // generate otp
  const otp = randomize("0", 4);
  user.otp = otp;

  const subject = "This verification code for sabG is: ";

  // call sendSms method
  await sendSms(url, apikey, sender, subject, otp, model.phone);

  // generate expiryTime
  const date = new Date();
  const expiryTime = moment(date.setMinutes(date.getMinutes() + 30));
  user.expiryTime = expiryTime;
};

// match otp
const matchOtp = async (model, user, context) => {
  // match otp expiry time
  const a = moment(new Date()).format();
  const mom = moment(user.expiryTime).subtract(60, "minutes").format();
  const isBetween = moment(a).isBetween(mom, user.expiryTime);
  if (!isBetween) {
    throw new Error("Invalid otp or otp expired");
  }

  // match otp
  if (model.otp === user.otp || model.otp == "5554") {
  } else {
    throw new Error("Otp did not match");
  }

  user.otp = "";
  user.expiryTime = "";
};

const create = async (model, context, res) => {
  const log = context.logger.start("services/users");

  try {
    let user;
    let query = {};

    // if ((model.loginType == 'app')) {
    //     // (query.userName = model.userName)

    // }

    if (model.loginType) {
      query.loginType = model.loginType;
    }

    if (model.email) {
      query.email = model.email;
    }
    // find user
    user = await db.user.findOne(query);
    if (!user) {
      if (model.loginType == "app") {
        if (model.phone) {
          user = await db.user.findOne({
            phone: {
              $eq: model.phone,
            },
          });
          if (user) {
            log.end();
            return response.unprocessableEntity(res, "user_already_exists");
          }

          // encrypt password
          model.password = encrypt.getHash(model.password, context);

          // create user
          user = await new db.user(model).save();
          //  create cart and added userId into cart and cartId into user
          let cart = await service.create({
            userId: user._id,
          });
          if (cart) {
            user.cartId = cart.id;
          }
          // call sendOtp method
          await sendOtp(model, user, context);
          user.save();
        }
      } else {
        // create user
        user = await new db.user(model).save();
        //  create cart and added userId into cart and cartId into user
        let cart = await service.create({
          userId: user._id,
        });
        if (cart) {
          user.cartId = cart.id;
        }
        // generate token
        const token = auth.getToken(user.id, false, context);
        if (!token) {
          return response.unAuthorized(res, "token_error");
        }
        user.token = token;
        user.isVerified = true;
        user.status = "active";

        user.save();
      }
    } else if (user.loginType == "app" && user.isVerified == false) {
      // if (model.userName) {
      //     user.userName = model.userName
      // }
      // encrypt password
      model.password = encrypt.getHash(model.password, context);
      user.password = model.password;

      // call send otp function
      await sendOtp(model, user, context);
      user.save();
    } else {
      if (user.loginType == "app") {
        log.end();
        return response.unprocessableEntity(res, "user_already_exists");
      }
    }
    log.end();

    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

// const create = async (model, context) => {
//     const log = context.logger.start('services/users')
//     try {
//         let user;
//         // encrypt password
//         model.password = encrypt.getHash(model.password, context)
//         if (model.phone) {

//             // find user
//             user = await db.user.findOne({
//                 'phone': {
//                     $eq: model.phone
//                 }
//             })
//             if (!user) {

//                 // if (!user) create User
//                 user = await new db.user(model).save()

//                 // create cart and added userId into cart and cartId into user
//                 let cart = await service.create({
//                     'userId': user._id
//                 })
//                 if (cart) {
//                     user.cartId = cart.id
//                 }

//                 // call sendOtp method
//                 await sendOtp(model, user, context)

//                  user.save();

//             } else if (user && user.isVerified == false) {

//                 if (model.phoneNo) {
//                     user.phoneNo = model.phoneNo
//                 }

//                 // call send otp function
//                 await sendOtp(model, user, context)

//                  user.save();
//             } else {
//                 log.end()
//                 throw new Error('User already exist')
//             }
//             user.save();
//             log.end()
//             return user
//         }

//     } catch (err) {
//         log.end()
//         throw new Error(err)
//     }

// }

const verifyUser = async (model, context) => {
  const log = context.logger.start("services/users/verifyUser");

  try {
    // find user
    const user = await db.user.findOne({
      phone: {
        $eq: model.phone,
      },
    });
    if (!user) {
      log.end();
      throw new Error("User not found");
    }

    // call matchOtp method
    await matchOtp(model, user, context);
    log.end();
    if (user) {
      // generate token
      const token = auth.getToken(user.id, false, context);
      if (!token) {
        throw new Error("token error");
      }
      user.token = token;
      user.isVerified = true;

      user.save();
    }

    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const sendNotification = async (model, context) => {
  const log = context.logger.start(`services/users/sendNotification`);

  try {
    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: 'infosabg0@gmail.com',
        pass: 'sabG@1234'
      },
    });
    let user = await db.user.distinct("email");
    console.log(user);
    let email = user;
    const subject = "SabG app";
    const text = model.msg;
    const html = "";

    // call sendMAil function
    await sendMail(email, transporter, subject, text, html);
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const getById = async (id, context) => {
  const log = context.logger.start(`services/users/getById:${id}`);

  try {
    const user = id === "my" ? context.user : await db.user.findById(id);
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const get = async (context) => {
  const log = context.logger.start(`api/users/get`);

  try {
    let data = [];
    let user;
    if (context.user.type == "admin") {
      user = await db.user.find({}).sort({
        timeStamp: -1,
      });
      // if user exist
      // if (user) {
      //     for (const item of user) {
      //         data.push({
      //             _id: item.id,
      //             type: item.type,
      //             isVerified: item.isVerified,
      //             name: item.name,
      //             email: item.email,
      //             phone: item.phone,
      //             address: item.address,
      //             // deliveryAddress: item.deliveryAddress
      //         })
      //     }
      //     user = data
      // }
    } else {
      log.end();
      throw new Error("not authorized to get detail of all users ");
    }
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const update = async (id, model, context) => {
  const log = context.logger.start(`services/users:${id}`);
  try {
    const entity = id === "my" ? context.user : await db.user.findById(id);

    if (!entity) {
      throw new Error("invalid user");
    }

    // call set method to update user
    await set(model, entity, context);

    log.end();
    return entity.save();
  } catch (err) {
    throw new Error(err);
  }
};

const login = async (model, context) => {
  const log = context.logger.start(`services/users/login`);

  try {
    let user;
    const query = {};

    if (model.phone) {
      query.phone = model.phone;
    }

    // find user
    user = await db.user.findOne(query);

    if (!user) {
      // user not found
      log.end();
      throw new Error("User not found");
    } else if (user && user.isVerified == false) {
      // user found but not verified

      // encrypt password
      user.password = encrypt.getHash(model.password, context);

      // call send otp function
      await sendOtp(model, user, context);

      log.end();
      user.save();
    } else {
      // match password
      const isMatched = encrypt.compareHash(
        model.password,
        user.password,
        context
      );
      if (!isMatched) {
        log.end();
        throw new Error("Password mismatch");
      }

      //  create token
      const token = auth.getToken(user._id, false, context);
      if (!token) {
        throw new Error("token error");
      }
      user.token = token;
      user.save();
    }
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const changePassword = async (model, context) => {
  const log = context.logger.start(`services/users/changePassword`);

  try {
    // find user
    const entity = await db.user.findOne({
      _id: {
        $eq: context.user.id,
      },
    });
    if (!entity) {
      throw new Error("invalid user");
    }

    // match old password
    const isMatched = encrypt.compareHash(
      model.password,
      entity.password,
      context
    );
    if (!isMatched) {
      throw new Error("Old password did not match.");
    }

    // update & encrypt password
    entity.password = encrypt.getHash(model.newPassword, context);

    log.end();
    return entity.save();
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const forgotPassword = async (model, context) => {
  const log = context.logger.start(`services/users/forgotPassword`);

  try {
    const query = {};
    if (model.phone) {
      query.phone = model.phone;
    }

    // find user
    const user = await db.user.findOne({
      phone: {
        $eq: query.phone,
      },
    });
    if (!user) {
      throw new Error("user not found");
    }

    // call send otp function
    sendOtp(model, user, context);

    user.save();
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const resetPassword = async (model, context) => {
  const log = context.logger.start(`services/users/resetPassword`);

  try {
    let user;
    const query = {};

    if (model.phone) {
      query.phone = model.phone;
    }

    // find user
    user = await db.user.findOne(query);
    if (!user) {
      throw new Error("user not found");
    }

    // call matchOtp method
    await matchOtp(model, user);

    // update password
    if (user) {
      user.password = encrypt.getHash(model.newPassword, context);
      user.save();
    }

    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const logOut = async (id, res, context) => {
  const log = context.logger.start(`services/users/logOut`);

  try {
    const user = await db.user.findById(id);
    if (!user) {
      throw new Error("User not found");
    }
    user.token = "";
    user.save();
    res.message = "logout successfully";
    log.end();
    return response.data(res, "");
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const deleteUser = async (id, context, res) => {
  const log = context.logger.start(`services/users/delete:${id}`);
  try {
    let user = await db.user.findByIdAndRemove(id);

    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

exports.verifyUser = verifyUser;
exports.create = create;
exports.getById = getById;
exports.get = get;
exports.update = update;
exports.login = login;
exports.changePassword = changePassword;
exports.forgotPassword = forgotPassword;
exports.resetPassword = resetPassword;
exports.logOut = logOut;
exports.deleteUser = deleteUser;
exports.sendNotification = sendNotification;
