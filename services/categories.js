'use strict'
// const service = require('./products')
const response = require('../exchange/response')
const createTempcategoryObj = async (category, skipCount) => {
    var categoryObj = {
        category: category,

        skipCount: skipCount,

    }

    return categoryObj;
}


const set = (model, entity, context) => {
    const log = context.logger.start('services/categories/set')
    try {
        if (model.image) {
            entity.image = model.image
        }
        if (model.name) {
            entity.name = (model.name).toLowerCase()
        }
        if(model.status){
            entity.status=model.status
        }
        log.end()
        return entity
    } catch (err) {
        throw new Error(err)
    }
}
const create = async (model, context) => {
    const log = context.logger.start(`services/categories`)

    try {
        let category
        let name=(model.name).toLowerCase()
        if (name) {

            // find user 
            category = await db.category.findOne({
                'name': {
                    $eq: name
                }
            })

            if (category) {
                log.end()
                throw new Error('category already exist')
                
            } else {
                category = await new db.category(model).save()
            }
            // return category

            log.end()
            return category
        }
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const update = async (id, model, context) => {
    const log = context.logger.start(`services/categories:${id}`)
    try {

        const entity = id === 'my' ? context.category : await db.category.findById(id)

        if (!entity) {
            throw new Error('invalid user')
        }

        // call set method to update user
        await set(model, entity, context)

        log.end()
        return entity.save()
    } catch (err) {
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/categories/getById:${id}`)
    try {
        const catagory = await db.category.findById(id)
        log.end()
        return catagory
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req,context) => {
    const log = context.logger.start(`services/categories/get`)
    // try {
    //     const catagory = await db.category.find()
    //     log.end()
    //     return catagory
    // } catch (err) {
    //     log.end()
    //     throw new Error(err)
    // }
    try {
        let category
        const params = req.query;
        let pageNo = Number(params.pageNo) || 1
        let items = Number(params.items) || 10
        let skipCount = items * (pageNo - 1)


        if (params  && (params.status != undefined && params.status != null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {
            category = await db.category.find({
                'status': {
                    $eq: params.status
                }
            }).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })
            const tempcategoryResponseObj = await createTempcategoryObj(category, skipCount)
            category = tempcategoryResponseObj




        } else {
            category = await db.category.find({}).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })
            const tempcategoryResponseObj = await createTempcategoryObj(category, skipCount)
            category = tempcategoryResponseObj

        }
        log.end()
        return category

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const deletecategory = async (id, context, res) => {
    const log = context.logger.start(`services/categories/delete:${id}`)
    try {
        const catagory = await db.category.findByIdAndRemove(id)
        log.end()


        // if (catagory) {
        //     await db.catagory.deleteOne(id)

        //  }



        // log.end()
        log.end()
        return catagory
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.deletecategory = deletecategory

