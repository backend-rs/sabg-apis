'use strict'

const set = async (model, req, entity, context) => {
    const log = context.logger.start('services/slider/set')
    try {
        if (model.image) {

            if (entity.images.length <= 3) {
                entity.images.push({
                    url: model.image.url,
                    thumbnail: model.image.thumbnail,
                    resize_url: model.image.resize_url,
                    resize_thumbnail: model.image.resize_thumbnail
                })

            } else {
                entity.images.shift()
                entity.images.push({
                    url: model.image.url,
                    thumbnail: model.image.thumbnail,
                    resize_url: model.image.resize_url,
                    resize_thumbnail: model.image.resize_thumbnail
                })
            }
        }


        log.end()
        return entity
    } catch (err) {
        throw new Error(err)
    }
}



const create = async (model, context) => {
    const log = context.logger.start(`services/slider`)
    try {
        let sliderImage = await new db.slider(model).save()
        log.end()
        return sliderImage
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const update = async ( req,id, model, context) => {
    const log = context.logger.start(`services/slider:${id}`)
    try {
        let sliderImages
        // let i=0000
        const entity = id === 'my' ? context.slider : await db.slider.findById(id)
        if (!entity) {
            throw new Error('invalid slider')
        }
        if (req != null && req != undefined) {
            sliderImages = await set(model, req, entity, context)
        } else {
            sliderImages = await db.slider.findById(id)
            if (sliderImages) {
                if (sliderImages.totalBills == undefined && sliderImages.totalBills === undefined) {
                    sliderImages.totalBills = 0;
                    sliderImages.totalBills = sliderImages.totalBills + 1;
                } else {
                    sliderImages.totalBills += 1
                }
            }
        }

        log.end()
        return sliderImages.save()
    } catch (err) {
        throw new Error(err)
    }
}
// const update = async (req, id, body, context) => {
//     try {
//         let i = 0000
//         let file
//         const entity = await db.file.findById(id)
//         if (!entity) {
//             throw new Error('invalid product')
//         }

//         if (req != null && req != undefined) {
//             file = await set(body, req, entity, context)
//         } else {

//             file = await db.file.findById(id)

//             if (file) {
//                 if (file.totalBills == undefined && file.totalBills == null) {
//                     file.totalBills = 0;
//                     file.totalBills = file.totalBills + 1;
//                 } else {
//                     file.totalBills += 1
//                 }
//             }
//         }

//         return file.save()
//     } catch (err) {
//         throw new Error(err)
//     }
// }


const getById = async (id, context) => {
    const log = context.logger.start(`services/slider/getById:${id}`)
    try {

        const sliderImages = await db.slider.findById(id)
        log.end()
        return sliderImages

    } catch (err) {
        throw new Error(err)
    }
}
const get = async (req, context) => {
    const log = context.logger.start(`services/slider/get`)
    try {
        let params = req.query
        let sliderImages
        let temp = {}
        let pageNo = Number(params.pageNo) || 1
        let items = Number(params.items) || 10
        let skipCount = items * (pageNo - 1)

        if (params && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {

            let index = JSON.parse(params.items)


            sliderImages = await db.slider.find({}).skip(skipCount).limit(items)

            for (const item of sliderImages) {
                if (item) {
                    temp = item.images[index - 1]
                }
            }
            sliderImages = temp

        } else {
            sliderImages = await db.slider.find({}).skip(skipCount).limit(items)
        }
        log.end()
        return sliderImages
    } catch (err) {
        throw new Error(err)
    }
}



exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
