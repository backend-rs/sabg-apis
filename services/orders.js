"use strict";
const service = require("../services/users");
const randomize = require("randomatic");
const services = require("../services/slider");
const nodemailer = require("nodemailer");
const dateFormat = require("dateformat");
const https = require("axios");
const moment = require("moment");
const { model } = require("mongoose");
const response = require("../exchange/response");

const apikey = "u80rypfvfJc-dLn4G9htETrHLToVF9ziofaSGiU0Jc";
const url = "https://api.textlocal.in/send";
const sender = "SABJEE";

const createTempOrderObj = async (order, count, skipCount, totalCount) => {
  var orderObj = {
    orders: order,
    count: count,
    skipCount: skipCount,
    totalCount: totalCount,
  };
  return orderObj;
};

// send sms method
const sendSms = async (url, apikey, sender, subject, phone) => {
  let data;

  data =
    "apikey=" +
    apikey +
    "&sender=" +
    sender +
    "&numbers=" +
    phone +
    "&message=" +
    subject;
  https
    .post(`https://api.textlocal.in/send?${data}`, {})
    .then((response) => {
      console.log(response);
    })

    .catch((error) => {
      console.log(error);
    });
};
const set = (model, entity, context) => {
  const log = context.logger.start("services/orders/set");
  try {
    if (model && model.status) {
      if (model.status == "rejected") {
        entity.status = model.status;
      } else {
        entity.status = model.status;
      }
    }

    log.end();
    return entity;
  } catch (err) {
    throw new Error(err);
  }
};
const sendMail = async (email, transporter, subject, text, html) => {
  const details = {
    from: "infosabg0@gmail.com",
    to: email, // Receiver's email id
    subject: subject,
    text: text,
    html: html,
  };

  var info = await transporter.sendMail(details);
  console.log("INFO:::", info);
};
const create = async (model, context) => {
  const log = context.logger.start(`services/orders/create`);
  try {
    let order;
    order = await new db.order(model).save();
    if (!model.cart._id) {
      throw new Error("Cart id is required");
    }
    let cart = await db.cart.findById(model.cart._id);
    if (!cart) {
      throw new Error("Invalid cart");
    }
    let data = [];

    let grandTotal = 0;
    for (let item of cart.products) {
      let product = await db.product.findById(item._id);
      let total = item.quantity * item.price;
      data.push({
        _id: product._id,
        name: product.name,
        image: product.image,
        quantity: item.quantity,
        dimensions: item.dimensions,
        price: item.price,
        marketPrice: item.marketPrice,
        total: total,
      });
      grandTotal = grandTotal + total;
    }
    order.products = data;
    if (grandTotal < 200) {
      order.deliveryCharge = 20;
      order.grandTotal = grandTotal + order.deliveryCharge;
    } else {
      order.deliveryCharge = 0;
      order.grandTotal = grandTotal;
    }

    // find user
    let user = await service.getById(cart.userId, context);

    let months = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"];
    let datetemp = moment(new Date()).format("D-M-YYYY");
    // new date
    let dateObj = new Date();

    var date = dateObj.getDate();
    date = date < 10 ? `0${date}` : date;

    var month = dateObj.getMonth();
    let tempMonth = months[month];

    let year = dateObj.getFullYear();
    let tempYear = year.toString().substr(-2);

    // generate random number
    const randomNumber = randomize("0", 3);

    // generated orderId
    let orderId = tempYear + tempMonth + date + randomNumber;

    // get firstName ,lastNAme and phone from deliveryAddress
    order.firstName = user.profile.deliveryAddress.firstName;
    order.lastName = user.profile.deliveryAddress.lastName;
    order.phone = user.profile.deliveryAddress.contactNumber;
    order.deliveryAddress = user.profile.deliveryAddress;
    order.cartId = user.cartId;
    order.orderId = orderId;
    order.date = dateObj;
    order.orderDate = datetemp;

    order.save();

    cart.products = [];
    cart.save();

    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "infosabg0@gmail.com",
        pass: "sabG@1234",
      },
    });
    const email = "infosabg0@gmail.com";

    if (
      (order.firstName == undefined || order.firstName == null) &&
      (order.lastName == undefined || order.lastName == null)
    ) {
      order.firstName = " ";
      order.lastName = " ";
    }
    const subject = "New order received:";
    const text = "New order received:";
    const html =
      "<b>OrderId:</b>" +
      " " +
      order.orderId +
      "<br>" +
      "<b>Order by: </b>" +
      " " +
      (order.firstName.charAt(0).toUpperCase() + order.firstName.slice(1)) +
      " " +
      (order.lastName.charAt(0).toUpperCase() + order.lastName.slice(1)) +
      // order.firstName +" " +order.lastName +
      "<br>" +
      "<b>Contact No:</b>" +
      " " +
      order.phone;

    // call sendMAil function
    await sendMail(email, transporter, subject, text, html);

    log.end();
    return order;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};
const update = async (id, model, context) => {
  const log = context.logger.start(`services/orders:${id}`);
  try {
    let entity = id === "my" ? context.order : await db.order.findById(id);
    if (!entity) {
      throw new Error("invalid order");
    }

    // call set method to update user
    let order = await set(model, entity, context);

    if (entity.firstName == undefined) {
      entity.firstName = " ";
    }

    if (entity.comment == undefined) {
      entity.comment = " ";
    }

    if (entity.deliveryAddress.city == undefined) {
      entity.deliveryAddress.city = " ";
    }
    if (entity.deliveryAddress.apartmentName == undefined) {
      entity.deliveryAddress.apartmentName = " ";
    }
    if (entity.deliveryAddress.addressLine === undefined) {
      entity.deliveryAddress.addressLine = " ";
    }
    if (entity.deliveryAddress.floor === undefined) {
      entity.deliveryAddress.floor = " ";
    }
    if (
      entity.deliveryAddress.city === undefined &&
      entity.deliveryAddress.addressLine === undefined &&
      entity.deliveryAddress.apartmentName === undefined &&
      entity.deliveryAddress.floor === undefined
    ) {
      (entity.deliveryAddress.city = " ") &&
        (entity.deliveryAddress.addressLine = " ") &&
        (entity.deliveryAddress.appartmentName = "") &&
        (entity.deliveryAddress.floor = " ");
    }

    order.save();

    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "infosabg0@gmail.com",
        pass: "sabG@1234",
      },
    });
    let user = await db.user.findOne({
      cartId: {
        $eq: order.cartId,
      },
    });
    if (!user) {
      throw new Error("User not found");
    }
    const orderStatus =
    "<p>"+
      "Dear" +
      " " +
      entity.firstName +
      "," +
      "\n" +
      "Your order no." +
      " " +
      entity.orderId +
      " " +
      "has been" +
      " " +
      order.status +
      " " +
      "of amount is Rs" +
      " " +
      order.grandTotal +
      "." +
      "<br>" +"Regards" +"," +"<br>" +"sabG"+
     " </p>";
    const subject = "Order status:";
    const text = "Order status:";

    await sendMail(user.email, transporter, subject, text, orderStatus);

    // log.end()
    // order.save()

    if (model.status == "confirmed") {
      let req = null;
      let file;

      // find file
      file = await services.update(req, model._id, model, context);

      // function for 4 digit invoice number
      var invoiceNo = file.totalBills.toString();
      if (invoiceNo.length < 4) {
        var n = 4 - invoiceNo.length;
        for (var i = 0; i < n; i++) {
          invoiceNo = "0" + invoiceNo;
        }
      }

      const email = "infosabg0@gmail.com";
      // var elementSkelton;
      var productElements;
      var temp = "";

      // get products detail from order
      for (var i = 0; i < order.products.length; i++) {
        var j = i + 1;
        productElements =
          "<tr>" +
          "<td style='border:1px solid black'>" +
          j +
          "</td>" +
          "<td style='border:1px solid black'>" +
          order.products[i].name +
          "</td>" +
          "<td style='border:1px solid black'>" +
          order.products[i].price +
          "</td>" +
          "<td style='border:1px solid black'>" +
          order.products[i].dimensions.value +
          " " +
          order.products[i].dimensions.unit +
          "</td>" +
          "<td style='border:1px solid black'>" +
          order.products[i].quantity +
          "</td>" +
          "<td style='border:1px solid black'>" +
          order.products[i].total +
          "</td>" +
          "</tr>";
        temp = temp + productElements;
      }

      // format of invoice(form) send to admin through mail
      const orderDetails =
        "<table style='border:1px solid black;border-collapse: collapse;font-size:10px'>" +
        "<tr ><td colspan='8' style='border:1px solid black'><center><b style='font-size:12px'>INVOICE</b></center></td></tr>" +
        "<tr>" +
        "<td rowspan='2' style='border:1px solid black'><b>Address</b></td>" +
        "<td rowspan='2' colspan='2' style='border:1px solid black'>Pipal wali gali, Chahal chowk, Shri Kanha tent house, Sri Ganganagar (Rajasthan), 335001</td>" +
        "<td colspan='2' style='border:1px solid black'><b>Invoice no.</b></td>" +
        "<td colspan='2' style='border:1px solid black;min-width:4ch'>" +
        invoiceNo +
        "</td>" +
        "</tr>" +
        "<tr>" +
        "<td colspan='2' style='border:1px solid black'><b>Order id</b></td>" +
        "<td colspan='2' style='border:1px solid black'>" +
        order.orderId +
        "</td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border:1px solid black'><b>Email</b></td>" +
        "<td colspan='7' style='border:1px solid black'>infosabg0@gmail.com</td>" +
        // "<td colspan='2'  style='border:1px solid black'><b>GST no.</b></td>" + "<td colspan='2' style='border:1px solid black'> </td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border:1px solid black'><b>Mobile</b></td>" +
        "<td colspan='2' style='border:1px solid black'>8058888838</td>" +
        "<td colspan='2' style='border:1px solid black'><b>Date</b></td>" +
        "<td colspan='2' style='border:1px solid black'>" +
        dateFormat(new Date(), "dd.mm.yyyy") +
        "</td>" +
        "</tr>" +
        "</tr>" +
        // customer details
        "<tr>" +
        "<tr>" +
        "<td style='border:1px solid black'><b>Customer name</b></td>" +
        "<td colspan='2' style='border:1px solid black'>" +
        order.firstName +
        "</td>" +
        "<td colspan='2' style='border:1px solid black'><b>Phone</b></td>" +
        "<td colspan='4' style='border:1px solid black'>" +
        order.deliveryAddress.contactNumber +
        "</td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border:1px solid black'><b>Address </b></td>" +
        "<td colspan='7' style='border:1px solid black'>" +
        order.deliveryAddress.addressLine +
        order.deliveryAddress.city +
        order.deliveryAddress.floor +
        " " +
        "</td>" +
        "</tr>" +
        "</tr>" +
        // products details
        "<tr>" +
        "<tr> <td style='border:1px solid black'><b>Sr. no.</b></td>  <td style='border:1px solid black'><b>Product name</b></td> <td style='border:1px solid black'><b>Price(Rs)</b></td>  <td style='border:1px solid black'><b>Weight/Piece</b></td>   <td style='border:1px solid black'><b>Qty</b></td>   <td style='border:1px solid black'><b>Amount(Rs)</b></td> </tr>" +
        temp +
        "<tr>" +
        "<tr>" +
        "<td colspan='3' style='border:1px solid black'><center><b>Delivery charges(Rs)</b></center></td>" +
        "<td colspan='4' style='border:1px solid black'><center>" +
        order.deliveryCharge +
        "</td>" +
        "</tr>" +
        "<td colspan='3' style='border:1px solid black'><center><b>GRAND TOTAL</b></center></td>" +
        "<td colspan='4' style='border:1px solid black'><center>" +
        order.grandTotal +
        "</center></td>" +
        "</tr>" +
        "</tr>" +
        // query
        "<tr>" +
        "<tr>" +
        "<td  colspan='7' style='border:1px solid black;padding-top:10%'><p style='float:right'><img src='http://54.179.179.175:2000/files/sabG_Logo-1577519976243.png' height='40px' width='60px'><br><b>THANK YOU!</b><br>for shopping with us</p></td>" +
        "</tr>" +
        "</table>";
      // "<form>"+
      // "<input type='button' value='print' onclick='window.print();'/>"+
      // "</form>"
      const subject = "Invoice:";
      const text = "Invoice:";

      // call sendMAil function
      await sendMail(email, transporter, subject, text, orderDetails);
    }
    log.end();
    return order;
  } catch (err) {
    throw new Error(err);
  }
};

const getById = async (id, req, context) => {
  const log = context.logger.start(`services/orders/getById:${id}`);
  try {
    let order;
    let data = [];
    let values = [];
    let params = req.query;

    // find order
    order = await db.order.findById(id);
    data = order.products;

    if (
      params &&
      params.pageNo != undefined &&
      params.pageNo != null &&
      params.items != undefined &&
      params.items != null
    ) {
      let pageNo = Number(params.pageNo) || 1;
      let items = Number(params.items) || 10;
      let skipCount = items * (pageNo - 1);

      values = data.skip(skipCount).limit(items);
      // total products
      let totalCount = data.length;

      // total skipped
      let skippedCount = skipCount;

      // requested items
      let count = items;

      order.products = values;
      order.totalCount = totalCount;
      order.skipCount = skippedCount;
      order.count = count;
    } else {
      order.products = data;
      order.totalCount = "";
      order.skipCount = "";
      order.count = "";
    }

    log.end();
    return order;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};
const get = async (req, context, res) => {
  const log = context.logger.start(`services/orders/get`);
  try {
    let order;
    const params = req.query;
    let pageNo = Number(params.pageNo) || 1;
    let items = Number(params.items) || 10;
    let skipCount = items * (pageNo - 1);

    if (params.orderDate != null && params.orderDate != undefined) {
      let order = await db.order.aggregate([
        { $match: params },
        { $unwind: "$products" },
        {
          $group: {
            _id: {
              id: "$products._id",
              name: "$products.name",
              price: "$products.price",
              unit: {
                $cond: {
                  if: { $eq: ["$products.dimensions.unit", "piece"] },
                  then: "piece",
                  else: "kg",
                },
              },
            },
            count: {
              $sum: {
                $multiply: [
                  "$products.quantity",
                  {
                    $cond: {
                      if: { $eq: ["$products.dimensions.unit", "g"] },
                      then: {
                        $divide: [
                          { $toInt: "$products.dimensions.value" },
                          1000,
                        ],
                      },
                      else: { $toInt: "$products.dimensions.value" },
                    },
                  },
                ],
              },
            },
          },
        },
      ]);
      console.log(order);

      if (order && order.length > 0) {
        // var elementSkelton;
        var productElements;
        var temp = "";
        // get products detail from order
        for (var i = 0; i < order.length; i++) {
          var j = i + 1;
          productElements =
            "<tr>" +
            "<td style='border:1px solid black'>" +
            j +
            "</td>" +
            "<td colspan='2' style='border:1px solid black'>" +
            (order[i]._id.name.charAt(0).toUpperCase() +
              order[i]._id.name.slice(1)) +
            // order[i]._id.name +
            "</td>" +
            "<td colspan='4' style='border:1px solid black'>" +
            order[i].count +
            " " +
            order[i]._id.unit +
            "</td>" +
            "</tr>";
          temp = temp + productElements;
        }

        var transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: "infosabg0@gmail.com",
            pass: "sabG@1234",
          },
        });

        // format of invoice(form) send to admin through mail
        const orderDetails =
          "<table style='border:1px solid black;border-collapse: collapse;font-size:10px'>" +
          "<tr ><td colspan='8' style='border:1px solid black'><center><b style='font-size:12px'>ORDER DETAILS</b></center></td></tr>" +
          "<tr>" +
          "<td style='border:1px solid black'><b>Email</b></td>" +
          "<td colspan='7' style='border:1px solid black'>infosabg0@gmail.com</td>" +
          "</tr>" +
          "<tr>" +
          "<td style='border:1px solid black'><b>Mobile</b></td>" +
          "<td colspan='2' style='border:1px solid black'>8058888838</td>" +
          "<td colspan='2' style='border:1px solid black'><b>Date</b></td>" +
          "<td colspan='2' style='border:1px solid black'>" +
          dateFormat(new Date(), "dd.mm.yyyy") +
          "</td>" +
          "</tr>" +
          "</tr>" +
          // products details
          "<tr>" +
          "<tr> <td style='border:1px solid black'><b>Sr. no.</b></td>  <td  colspan='2' style='border:1px solid black'><b>Product name</b></td>   <td colspan='4' style='border:1px solid black'><b>Weight/Piece</b></td>   </tr>" +
          temp +
          "<tr>" +
          "<tr>" +
          // query
          "<tr>" +
          "<tr>" +
          "<td  colspan='7' style='border:1px solid black;padding-top:10%'><p style='float:right'><img src='http://54.179.179.175:2000/files/sabG_Logo-1577519976243.png' height='40px' width='60px'><br><b>THANK YOU!</b><br>for shopping with us</p></td>" +
          "</tr>" +
          "</table>";
        // "<form>"+
        // "<input type='button' value='print' onclick='window.print();'/>"+
        // "</form>"
        const subject = "Your order details are:";
        const text = "Your order details are:";
        const email = "infosabg0@gmail.com";

        // call sendMAil function
        await sendMail(email, transporter, subject, text, orderDetails);
      }

      const tempOrderResponseObj = await createTempOrderObj(order, "", "", "");
      order = tempOrderResponseObj;
      return order;
    }

    if (
      params &&
      params._id != undefined &&
      params._id != null &&
      params.history != undefined &&
      params.history != null &&
      params.history == "true" &&
      params.pageNo != undefined &&
      params.pageNo != null &&
      params.items != undefined &&
      params.items != null
    ) {
      order = await db.order
        .find({
          cartId: {
            $eq: params._id,
          },
          status: ["delivered", "cancelled", "rejected"],
        })
        .skip(skipCount)
        .limit(items)
        .sort({
          timeStamp: -1,
        });

      // total orders
      let totalCount = await db.order
        .find({
          cartId: {
            $eq: params._id,
          },
          status: ["delivered", "cancelled", "rejected"],
        })
        .count();

      // total skipped
      let skippedCount = skipCount;

      // requested items
      let count = items;

      const tempOrderResponseObj = await createTempOrderObj(
        order,
        count,
        skippedCount,
        totalCount
      );
      order = tempOrderResponseObj;
    } else if (
      params &&
      params._id != undefined &&
      params._id != null &&
      params.history != undefined &&
      params.history != null &&
      params.history == "false" &&
      params.pageNo != undefined &&
      params.pageNo != null &&
      params.items != undefined &&
      params.items != null
    ) {
      order = await db.order
        .find({
          cartId: {
            $eq: params._id,
          },
          status: ["pending", "confirmed"],
        })
        .skip(skipCount)
        .limit(items)
        .sort({
          timeStamp: -1,
        });

      // total orders
      let totalCount = await db.order
        .find({
          cartId: {
            $eq: params._id,
          },
          status: ["pending", "confirmed"],
        })
        .count();

      // total skipped
      let skippedCount = skipCount;

      // requested items
      let count = items;

      const tempOrderResponseObj = await createTempOrderObj(
        order,
        count,
        skippedCount,
        totalCount
      );
      order = tempOrderResponseObj;
    } else {
      if (
        params.history != undefined &&
        params.history != null &&
        params.history == "false" &&
        params.pageNo != undefined &&
        params.pageNo != null &&
        params.items != undefined &&
        params.items != null
      ) {
        order = await db.order
          .find({
            status: ["pending", "confirmed"],
          })
          .skip(skipCount)
          .limit(items)
          .sort({
            timeStamp: -1,
          });
        let count = "";
        let skippedCount = "";
        let totalCount = "";

        const tempOrderResponseObj = await createTempOrderObj(
          order,
          count,
          skippedCount,
          totalCount
        );
        order = tempOrderResponseObj;
      } else if (
        params.history != undefined &&
        params.history != null &&
        params.history == "true" &&
        params.pageNo != undefined &&
        params.pageNo != null &&
        params.items != undefined &&
        params.items != null
      ) {
        order = await db.order
          .find({
            status: ["delivered", "cancelled", "rejected"],
          })
          .skip(skipCount)
          .limit(items)
          .sort({
            timeStamp: -1,
          });
        let count = "";
        let skippedCount = "";
        let totalCount = "";

        const tempOrderResponseObj = await createTempOrderObj(
          order,
          count,
          skippedCount,
          totalCount
        );
        order = tempOrderResponseObj;
      }
    }

    log.end();
    return order;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

exports.create = create;
exports.getById = getById;
exports.get = get;
exports.update = update;
