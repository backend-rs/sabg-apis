'use strict'
const service = require('./products')

const set = async (model, entity, context) => {
    const log = context.logger.start('services/carts/set')
    var index = -1;
    try {
        let quantity
        if (model.userId) {
            entity.userId = model.userId
        }

        if (model.product) {
            if (!model.product._id) {
                throw new Error("Product id is required")
            }
            let tempProduct = await service.getById(model.product._id, context)
            if (tempProduct) {

                if (entity.products && entity.products.length != 0) {


                    for (const product of entity.products) {
                        if (product.id == model.product._id) {
                            index = entity.products.indexOf(product);
                        }
                    }
                    if (index != -1) {
                        if (model.product.quantity == 0) {
                            entity.products.splice(index, 1);
                        } else {
                            entity.products[index] = {
                                _id: model.product._id,
                                marketPrice: tempProduct.marketPrice,
                                price: model.product.price,
                                dimensions: model.product.dimensions,
                                quantity: model.product.quantity,
                                total: model.product.quantity * model.product.price
                            }
                        }

                    } else {
                        if (!model.product.quantity == 0) {
                            entity.products.push({
                                _id: model.product._id,
                                marketPrice: tempProduct.marketPrice,
                                price: model.product.price,
                                dimensions: model.product.dimensions,
                                quantity: model.product.quantity,
                                total: model.product.quantity * model.product.price
                            })
                        }
                    }
                } else {
                    
                    quantity = JSON.parse(model.product.quantity)
                    if (!(quantity == 0 || quantity < 0 || quantity == undefined)) {
                        entity.products.push({
                            _id: model.product._id,
                            marketPrice: tempProduct.marketPrice,
                                price: model.product.price,
                                dimensions: model.product.dimensions,
                                quantity: model.product.quantity,
                                total: model.product.quantity * model.product.price

                        })

                    } else {
                        throw new Error("Quantity undefined or may be 0")
                    }
                }

            }

        }

        log.end()
        return entity
    } catch (err) {
        throw new Error(err)
    }
}


const create = async (model, context) => {
    // const log = context.logger.start('services/carts/create')
    try {
        let cart = await new db.cart(model).save()
        // log.end()
        return cart

    } catch (err) {
        throw new Error(err)
    }
}

const get = async (req, context) => {
    const log = context.logger.start('services/carts/get')
    let data = []
    let params = req.query
    let grandTotal = 0
    let cart
    try {
        // let products
        // const params = req.query;
        let pageNo = Number(params.pageNo) || 1
        let items = Number(params.items) || 10
        let skipCount = items * (pageNo - 1)


        if (params && (params._id != undefined && params._id != null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {
            cart = await db.cart.findOne({
                '_id': {
                    $eq: params._id
                }

            }).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })
            // const cart = await db.cart.findById(id)
            if (!cart) {
                throw new Error("Product id is required")
            }
            for (let item of cart.products) {
                if (!item.product) {
                    let product = await db.product.findById(item._id)
                    let total = item.quantity * item.price
                    data.push({
                        _id: product._id,
                        name: product.name,
                        image: product.image,
                        marketPrice: product.marketPrice,
                        dimensions: item.dimensions,
                        price: item.price,
                        quantity: item.quantity,
                        total: total.toPrecision(4)
                    })
                    grandTotal = grandTotal + total
                }
            }
            cart.products = data

            if (grandTotal < 200) {
                cart.deliveryCharge = 20
                cart.grandTotal = grandTotal+cart.deliveryCharge
                cart.grandTotal=cart.grandTotal.toPrecision(4)
            } else {
                cart.deliveryCharge = 0
                cart.grandTotal = grandTotal.toPrecision(4)
            }
    

            log.end()
            return cart

        }
    } catch (err) {
        throw new Error(err)
    }
}



const update = async (id, model, context) => {
    const log = context.logger.start('services/carts/update')

    try {
        // find cart by id
        const entity = await db.cart.findById(id)
        if (!entity) {
            throw new Error('Invalid cart')
        }
        // call set method
        let cart = await set(model, entity, context)
        cart.save();

        let data = []
        let grandTotal = 0
        for (let item of cart.products) {
            if (!item.product) {
                let product = await service.getById(item._id, context)
                let total = item.quantity * item.price
                data.push({
                    _id: product._id,
                    name: product.name,
                    image: product.image,
                    marketPrice: product.marketPrice,
                    dimensions: item.dimensions,
                    price: item.price,
                    quantity: item.quantity,
                    total: total.toPrecision(4)
                })
                grandTotal = grandTotal + total

            }

        }
        cart.products = data
        if (grandTotal < 200) {
            cart.deliveryCharge = 20
            cart.grandTotal = grandTotal+cart.deliveryCharge
        } else {
            cart.deliveryCharge = 0
            cart.grandTotal = grandTotal.toPrecision(4)
        }

        // cart.grandTotal = grandTotal.toPrecision(4)
        log.end()
        return cart

    } catch (err) {
        throw new Error(err)
    }
}

exports.create = create
exports.get = get
exports.update = update


