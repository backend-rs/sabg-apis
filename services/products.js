'use strict'
// const service = require('./products')
const response = require('../exchange/response')
const createTempProductObj = async (products, count, skipCount, totalCount) => {
    var productObj = {
        products: products,

        skipCount: skipCount,

    }

    return productObj;
}


const set = (model, entity, context) => {
    const log = context.logger.start('services/products/set')
    try {
        if (model.image) {
            entity.image = model.image
        }
        if (model.marketPrice) {
            entity.marketPrice = model.marketPrice
        }
        if (model.name) {
            entity.name = (model.name).toLowerCase()
        }
        if (model.price) {
            entity.price = model.price
        }
        // if (!entity.dimensions[0]) {
        //     if (body.dimensions && body.dimensions.value) {
        //         entity.dimensions[0] = body.dimensions
        //     }
        // } else {
        //     if (body.dimensions && body.dimensions.value) {
        //         entity.dimensions[0].value = body.dimensions.value
        //     }
        //     if (body.dimensions && body.dimensions.unit) {
        //         entity.dimensions[0].unit = body.dimensions.unit
        //     }
        // }
        if (model.dimensions) {
            entity.dimensions = model.dimensions

        }

        if (model.status) {
            entity.status = model.status
        }
        log.end()
        return entity
    } catch (err) {
        throw new Error(err)
    }
}
const create = async (model, context) => {
    const log = context.logger.start(`services/products/create`)
    try {
        let product
        let name=(model.name).toLowerCase()
        if (name) {

            // find user 
        product = await db.product.findOne({
                'name': {
                    $eq: name
                }
            })

            if (product) {
                log.end()
                throw new Error('category already exist')
                
            } else {
                product= await new db.product(model).save()
            }
            // return category

            log.end()
            return product
        }
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/products:${id}`)
    try {

        const entity = id === 'my' ? context.product : await db.product.findById(id)
        if (!entity) {
            throw new Error('invalid product')
        }

        // call set method to update user
        await set(model, entity, context)

        log.end()
        return entity.save()
    } catch (err) {
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/carts/getById:${id}`)

    try {
        const product = id === 'my' ? context.user : await db.product.findById(id)
        log.end()
        return product

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`services/products/get`)
    try {
        let products
        const params = req.query;
        let pageNo = Number(params.pageNo) || 1
        let items = Number(params.items) || 10
        let skipCount = items * (pageNo - 1)


        if (params && (params._id != undefined && params._id != null) && (params.status != undefined && params.status != null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {
            products = await db.product.find({
                'category._id': {
                    $eq: params._id
                },
                'status': {
                    $eq: params.status
                }
            }).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })
            const tempProductResponseObj = await createTempProductObj(products, skipCount)
            products = tempProductResponseObj




        } else {
            products = await db.product.find({
                'category._id': {
                    $eq: params._id
                }

            }).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })
            const tempProductResponseObj = await createTempProductObj(products, skipCount)
            products = tempProductResponseObj

        }
        log.end()
        return products

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const search = async (req, context) => {
    const log = context.logger.start('services/products/search')

    // let params = req.query

    //  let product
    try {
        let product
        const params = req.query;
        let pageNo = Number(params.pageNo) || 1
        let items = Number(params.items) || 10
        let skipCount = items * (pageNo - 1)
        let name = (params.name).toLowerCase()

        if (params && (params.name != undefined && params.name != null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {
            product = await db.product.find({
                name: {
                    // $regex: new RegExp("^" + name + "$", "i")
                    $regex:name,
                    $options:'i'
                }


            }).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })

            if (!product) {
                throw new Error("Product not found")
            }
            log.end()
            return product

        }
    } catch (err) {
        throw new Error(err)
    }
}
exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.search = search


