'use strict'
// const service = require('./products')
const response = require('../exchange/response')


const set = async (model, entity, context) => {
    const log = context.logger.start('services/offers/set')
    try {
        if (model.offer) {

            if (entity.offers.length == 0) {
                entity.offers.push({
                    heading: model.offer.heading,
                    description: model.offer.description

                })

            } else {
                entity.offers.shift()
                entity.offers.push({
                    heading: model.offer.heading,
                    description: model.offer.description
                })
            }
        }


        log.end()
        return entity
    } catch (err) {
        throw new Error(err)
    }
}
const create = async (model, context) => {
    const log = context.logger.start(`services/offers`)
    try {
        let offers = await new db.offer(model).save()
        // return catagory

        log.end()
        return offers
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/offers:${id}`)
    try {

        const entity = id === 'my' ? context.offer : await db.offer.findById(id)

        if (!entity) {
            throw new Error('invalid offer')
        }

        // call set method to update user
        await set(model, entity, context)

        log.end()
        return entity.save()
    } catch (err) {
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/offers/getById:${id}`)
    try {
        const offers = await db.offer.findById(id)
        log.end()
        return offers
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (context) => {
    const log = context.logger.start(`services/offers/get`)
    
    let offers
    
    try {
        offers = await db.offer.find({})
        if (!offers) {
            throw new Error('invalid offer')
        }

        log.end()
        return offers
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update


