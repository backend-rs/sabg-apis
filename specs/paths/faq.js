module.exports = [{
    url: '/',
   
    post: {
        summary: 'Create',
        description: 'Create slider',
        parameters: [{

            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in: 'body',
            name: 'body',
            description: 'Model of User creation',
            required: true,
            schema: {
                $ref: '#/definitions/faqCreateReq'
            },

        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
}

]