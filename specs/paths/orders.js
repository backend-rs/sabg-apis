module.exports = [
    {
        url: '/',
        get: {
            summary: 'Search',
            description: 'get order list',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'query',
                name: '_id',
                description: 'get order though cart',
                required: false,
                type: 'string'
            },
            {
                in: 'query',
                name: 'pageNo',
                description: 'get order though cart',
                required: false,
                type: 'number'
            },
            {

                in: 'query',
                name: 'orderDate',
                description: 'get order ',
                required: false,
                type: 'string'
            },
            {
                in: 'query',
                name: 'items',
                description: 'get order though cart',
                required: false,
                type: 'number'
            },
            {
                in: 'query',
                name: 'history',
                description: 'get order though true or false',
                required: false,
                type: 'string'
            },

        ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },
        post: {
            summary: 'Create',
            description: 'Create Product',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'body',
                name: 'body',
                description: 'Model of Product Creation',
                required: true,
                schema: {
                    $ref: '#/definitions/orderCreateReq'
                }
            }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get: {
            summary: 'Get',
            description: 'get order by Id',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'path',
                name: 'id',
                description: 'orderId',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },


        put: {
            summary: 'Update order',
            description: 'update product details',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'path',
                name: 'id',
                description: 'orderId',
                required: true,
                type: 'string'
            },
            {
                in: 'body',
                name: 'body',
                description: 'Model of order update',
                required: true,
                schema: {
                    $ref: '#/definitions/orderUpdateReq'
                }
            }
            ],
            responses: {
                default: {
                    description: {
                        schema: {
                            $ref: '#/definitions/Error'
                        }
                    }
                }
            }
        }
    }

]