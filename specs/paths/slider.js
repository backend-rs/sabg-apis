module.exports = [{
    url: '/',
    get: {
        summary: 'Search',
        description: 'get slider',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in: 'query',
            name: 'pageNo',
            description: 'to get slider images',
            required: false,
            type: 'number'
        },
        {
            in: 'query',
            name: 'items',
            description: 'to get slider images',
            required: false,
            type: 'number'
        }
    ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    post: {
        summary: 'Create',
        description: 'Create slider',
        parameters: [{

            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in: 'body',
            name: 'body',
            description: 'Model of User creation',
            required: true,
            schema: {
                $ref: '#/definitions/sliderCreateReq'
            },

        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
},
{
    url: '/{id}',
    get: {
        summary: 'Get',
        description: 'get slider by Id',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in: 'path',
            name: 'id',
            description: 'sliderId',
            required: true,
            type: 'string'
        }
        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    },


    put: {
        summary: 'Update slider image',
        description: 'update slider images',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in: 'path',
            name: 'id',
            description: '',
            required: true,
            type: 'string'
        },
        {
            in: 'body',
            name: 'body',
            description: 'Model of slider images update',
            required: true,
            schema: {
                $ref: '#/definitions/sliderUpdateReq'
            }
        }
        ],
        responses: {
            default: {
                description: {
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }
}



]