module.exports = [
    {
        url: '/',
        get: {
            summary: 'Search',
            description: 'get products list',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'query',
                name: '_id',
                description: 'get product though category',
                required: true,
                type: 'string'
            },
            {
                in: 'query',
                name: 'pageNo',
                description: 'get product though category',
                required: true,
                type: 'number'
            },
            {
                in: 'query',
                name: 'items',
                description: 'get product though category',
                required: true,
                type: 'number'
            },
            {
                in: 'query',
                name: 'status',
                description: 'get product though active or inactive',
                required: false,
                type: 'string'
            },

            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },

        post: {
            summary: 'Create',
            description: 'Create Product',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'body',
                name: 'body',
                description: 'Model of Product Creation',
                required: true,
                schema: {
                    $ref: '#/definitions/productCreateReq'
                }
            }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get: {
            summary: 'Get',
            description: 'get product by Id',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'path',
                name: 'id',
                description: 'productId',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },


        put: {
            summary: 'Update products',
            description: 'update product details',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'path',
                name: 'id',
                description: 'productId',
                required: true,
                type: 'string'
            },
            {
                in: 'body',
                name: 'body',
                description: 'Model of product update',
                required: true,
                schema: {
                    $ref: '#/definitions/productUpdateReq'
                }
            }
            ],
            responses: {
                default: {
                    description: {
                        schema: {
                            $ref: '#/definitions/Error'
                        }
                    }
                }
            }
        }
    },
    {
        url: '/search',
        get: {
            summary: 'Search',
            description: 'get products list',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'query',
                name: 'name',
                description: 'get product though name',
                required: true,
                type: 'string'
            },
            {
                in: 'query',
                name: 'pageNo',
                description: 'get product though name',
                required: true,
                type: 'number'
            },
            {
                in: 'query',
                name: 'items',
                description: 'get product though name',
                required: true,
                type: 'number'
            }


            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }

    }

]