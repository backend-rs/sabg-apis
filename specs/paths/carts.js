module.exports = [{
    url: '/',
    get: {
        summary: 'Search',
        description: 'get cart list',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in: 'query',
            name: '_id',
            description: 'cartId',
            required: true,
            type: 'string'
        },
        {
            in: 'query',
            name: 'pageNo',
            description: '',
            required: true,
            type: 'number'
        },
        {
            in: 'query',
            name: 'items',
            description: 'items',
            required: true,
            type: 'number'
        }
    ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
   
},
{
    url: '/{id}',
    

    put: {
        summary: 'Update cart',
        description: 'update cart details',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in: 'path',
            name: 'id',
            description: 'cartId',
            required: true,
            type: 'string'
        },
        {
            in: 'body',
            name: 'body',
            description: 'Model of cart update',
            required: true,
            schema: {
                $ref: '#/definitions/cartUpdateReq'
            }
        }
        ],
        responses: {
            default: {
                description: {
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }
}






]