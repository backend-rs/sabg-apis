module.exports = {
    // type: {
    //     type: String,
    //     enum: ['user', 'admin'],
    //     default: 'user'
    // },
    firstName: 'string',
    lastName: 'string',
    phone:'string',
    email: 'string',


    image: {
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string'
    },



    address: {
        city: 'string',
        addressLine: 'string',
        apartmentName: 'string',
        floor: 'string'
    },
    deliveryAddress: {
        firstName: 'string',
        lastName: 'string',
        contactNumber: 'string',
        city: 'string',
        addressLine: 'string',
        apartmentName: 'string',
        floor: 'string'
    }

}

