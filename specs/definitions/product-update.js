module.exports = {
    name: 'string',
    image: {
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string',
    },
    dimensions: {
    
        value:'string',
        unit:'string'
    },
    price: 'number',
    marketPrice: 'number',

    status: 'string'
        
        
    



}