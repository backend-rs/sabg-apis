'use strict'

const fs = require('fs')
const specs = require('../specs')
const api = require('../api')
var auth = require('../permit')
const validator = require('../validators')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

var multipart = require('connect-multiparty')
var multipartMiddleware = multipart()


const configure = (app, logger) => {
    const log = logger.start('settings:routes:configure')

    app.get('/specs', function (req, res) {
        fs.readFile('./public/specs.html', function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                })
            }
            res.contentType('text/html')
            res.send(data)
        })
    })

    app.get('/api/specs', function (req, res) {
        res.contentType('application/json')
        res.send(specs.get())
    })


    //-------------------- users routes ----------------------------
    app.post('/api/users/verifyUser', auth.context.builder, api.users.verifyUser);
    app.post('/api/users', auth.context.builder, validator.users.canCreate, api.users.create);

    app.get('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.getById, api.users.getById);
    app.get('/api/users', auth.context.builder, auth.context.requiresToken, api.users.get);

    app.put('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.update, api.users.update);
    app.post('/api/users/login', auth.context.builder, validator.users.login, api.users.login);
    app.post('/api/users/sendNotification', auth.context.builder,  api.users.sendNotification);

    app.post('/api/users/changePassword', auth.context.builder, auth.context.requiresToken, validator.users.changePassword, api.users.changePassword);
    app.post('/api/users/forgotPassword', auth.context.builder, api.users.forgotPassword);
    app.post('/api/users/resetPassword', auth.context.builder, api.users.resetPassword);

    app.post('/api/users/logOut/:id', auth.context.builder, api.users.logOut)
    app.delete('/api/users/:id', auth.context.requiresToken, api.users.delete);

    // ....................pushnotification-------------


    // ------------cart routes--------------------------
    app.get('/api/carts', auth.context.requiresToken, api.carts.get);
    app.get('/api/carts/:id', api.carts.get);
    app.put('/api/carts/:id', auth.context.builder, api.carts.update);


    // ----------------- upload files -----------------------
    app.post('/api/files', auth.context.builder, multipartMiddleware, api.files.create)
    app.post('/api/files/upload', multipartMiddleware, api.files.upload);
    app.get('/api/files/:id', auth.context.builder, api.files.getById)

    // -------------- category -------------------------------
    app.get('/api/categories/:id', auth.context.requiresToken, validator.categories.getById, api.categories.getById);
    app.post('/api/categories', auth.context.requiresToken, auth.context.builder, api.categories.create);
    app.get('/api/categories', auth.context.requiresToken, api.categories.get);
    app.put('/api/categories/:id', auth.context.requiresToken, validator.categories.update, api.categories.update);
    app.delete('/api/categories/delete/:id', auth.context.requiresToken, validator.categories.delete, api.categories.delete);

    // -------------- product -------------------------------
    app.get('/api/products/search', auth.context.requiresToken, api.products.search);


    app.get('/api/products/:id', auth.context.requiresToken, validator.products.getById, api.products.getById);
    app.post('/api/products', auth.context.requiresToken, auth.context.builder, api.products.create);
    app.get('/api/products', auth.context.requiresToken, api.products.get);
    app.put('/api/products/:id', auth.context.requiresToken, validator.products.update, api.products.update);

    // ---------------- slider -------------------------------
    app.get('/api/slider/:id', auth.context.requiresToken, validator.slider.getById, api.slider.getById);
    app.post('/api/slider', auth.context.requiresToken, auth.context.builder, api.slider.create);
    app.get('/api/slider', auth.context.requiresToken, api.slider.get);
    app.put('/api/slider/:id', auth.context.requiresToken, validator.slider.update, api.slider.update);


    //--------------------offers -----------------------------------
    app.get('/api/offers/:id', auth.context.requiresToken, api.offers.getById);
    app.post('/api/offers', auth.context.requiresToken, auth.context.builder, api.offers.create);
    app.get('/api/offers', auth.context.requiresToken, api.offers.get);
    app.put('/api/offers/:id', auth.context.requiresToken, api.offers.update);


    // ---------------------orders --------------------------------------
    app.get('/api/orders/:id', auth.context.requiresToken, api.orders.getById);
    app.post('/api/orders', auth.context.requiresToken, auth.context.builder, api.orders.create);
    app.get('/api/orders', auth.context.requiresToken, api.orders.get);
    app.put('/api/orders/:id', auth.context.requiresToken, api.orders.update);


    // ---------------------faq---------------------------------------
    app.post('/api/faq', auth.context.builder, auth.context.requiresToken, api.faq.create);


    log.end()
}

exports.configure = configure