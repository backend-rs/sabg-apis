'use strict'

// User Module
module.exports = {
    type: {
        type: String,
        default: 'user',
        enum: ['user', 'admin']
    },
    uId: String,
    tempToken: String,
    loginType: {
        type: String,
        default: 'app',
        enum: ['app', 'google', 'facebook']
    },
    phone: {
        type: String,
        unique: false,
    },
    userName:String,
    email:String,
    password: String,
    profile:{
        firstName:String,
        lastName:String,
        image:{
            url: String,
            thumbnail: String,
            resize_url: String,
            resize_thumbnail: String
        },
        address: {
            city:String,
            addressLine:String,
            apartmentName: String,
            floor:String
        },
        deliveryAddress:{
        
                firstName:String,
                lastName:String,
                contactNumber:String,
            city:String,
            addressLine:String,
            apartmentName: String,
            floor:String
        }
        
    },
    expiryTime: String,
    otp: String,
    isVerified:{
        type:Boolean,
        default:false
    },
    token: String, 
    cartId: String

}



