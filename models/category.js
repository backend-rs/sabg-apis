'use strict'
module.exports = {
    name:{ 
        type:String,
        lowercase: true,
    },
image: {
    url: String,
    thumbnail: String,
    resize_url: String,
    resize_thumbnail: String,
},
product:{
    _id:String
},
  
status: {
    type: String,
    default: 'active',
    enum: ['active', 'inactive']
}
}
