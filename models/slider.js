'use strict'
module.exports = {
    images:[{
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String
    }],
    totalBills:Number
}
