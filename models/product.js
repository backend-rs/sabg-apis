'use strict'
module.exports = {
    category: {
        _id: String
    },

    name:{ 
        type:String,
        lowercase: true,
    },
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String,
    },
    marketPrice: Number,
    price: Number,
    dimensions: {
        value: String,
        unit: String
    },


    
    status: {
        type: String,
        default: 'active',
        enum: ['active', 'inactive']
    }
}