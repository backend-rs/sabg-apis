'use strict'

module.exports = {
    userId: String,
    products: [{
        _id: String,
        
        name: String,
        
        image: {
            url: String,
            thumbnail: String,
            resize_url: String,
            resize_thumbnail: String,
        },
       marketPrice:String,
       price:String,
       dimensions: {
        value: String,
        unit: String
    },

        quantity: Number,
        total:Number
    }],
    deliveryCharge:Number,
    count: String,
    skipCount: String,
    totalCount: String,
    grandTotal: Number

}