'use strict'

module.exports = {

    status: {
        type: String,
        default: 'pending',
        enum: ['pending', 'confirmed', 'delivered', 'cancelled', 'rejected']
    },

    products: [{
        _id: String,
        name: String,
        image: {
            url: String,
            thumbnail: String,
            resize_url: String,
            resize_thumbnail: String,
        },


        marketPrice: Number,
        price: Number,
        dimensions: {
            value: String,
            unit: String
        },

        quantity: Number,
        total: Number
    }],
    deliveryCharge:Number,
    grandTotal: Number,
    cartId: String,
    orderId: String,
    firstName: String,
    lastName: String,
    phone: String,
    totalCount: String,
    skipCount: String,
    count: String,
    date: String,
    orderDate:String,
    comment:String,
    deliveryAddress: {

        firstName: String,
        lastName: String,
        contactNumber: String,

        city: String,
        addressLine: String,
        apartmentName: String,
        floor: String


    }



}
