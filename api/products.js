'use strict'

const response = require('../exchange/response')
const service = require('../services/products')
const mapper = require('../mappers/product')


exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/products`)

    try {
        const products= await service.create(req.body, req.context)
        log.end()
        return response.data(res, products)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/products/get/${req.params.id}`)
    try {
        const products = await service.getById(req.params.id, req.context)
        return response.data(res, products)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/products/get`)
    try {
        const products = await service.get(req,req.context)
        log.end()
        return response.data(res, products)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.update = async (req, res) => {
    const log = req.context.logger.start(`api/products/update${req.params.id}`)
    try {
        const products = await service.update( req.params.id, req.body, req.context)
        log.end()
        return response.data(res, products)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.search = async (req, res) => {
    const log = req.context.logger.start(`api/products/search`)
    try {
        const product= await service.search(req, req.context)
        log.end()
        return response.data(res, product)
    } catch (err) {
        log.end()
        return response.failure(res, err.message)
    }
}



// exports.delete = async (req, res) => {
//     const log = req.context.logger.start(`api/categories/delete${req.params.id}`)
//     try {
//         const category = await service.deletecategory(req.params.id, req.context)
//         log.end()
//         return response.data(res, 'successfully delete category')
//     } catch (err) {
//         log.error(err.message)
//         log.end()
//         return response.failure(res, err.message)
//     }
// }
// // exports.delete = async (req, res) => {
//     try {
//         const category = await service.delete(req)
//         return response.data(res, category)
//     } catch (err) {
//         return response.failure(res, err.message)
//     }
// }