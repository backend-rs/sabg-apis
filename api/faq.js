'use strict'

const response = require('../exchange/response')
const service = require('../services/faq')
// const mapper = require('../mappers/category')


exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/faq`)

    try {
        const faq= await service.create(req.body, req.context)
        log.end()
        return response.data(res, faq)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
