'use strict'

const response = require('../exchange/response')
const service = require('../services/offers')
const mapper = require('../mappers/offer')


exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/offers`)

    try {
        const offers = await service.create(req.body, req.context)
        log.end()
        return response.data(res, offers)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/offers/get/${req.params.id}`)
    try {
        const offers = await service.getById(req.params.id, req.context)
        return response.data(res, offers)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/offers/get`)
    try {
        const offers = await service.get(req.context)
        log.end()
        return response.data(res, offers)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.update = async (req, res) => {
    const log = req.context.logger.start(`api/offers/update${req.params.id}`)
    try {
        const offers = await service.update( req.params.id, req.body, req.context)
        log.end()
        return response.data(res, offers)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

