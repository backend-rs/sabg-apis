'use strict'

const response = require('../exchange/response')
const service = require('../services/orders')
const mapper = require('../mappers/order')


exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/orders`)

    try {
        const orders= await service.create(req.body, req.context)
        log.end()
        return response.data(res,orders)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/orders/get/${req.params.id}`)
    try {
        const orders= await service.getById(req.params.id, req.body,req.context)
        return response.data(res, orders)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/orders/get`)
    try {
        const orders= await service.get(req,req.context,res)
        log.end()
        return response.data(res, orders)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.update = async (req, res) => {
    const log = req.context.logger.start(`api/orders/update${req.params.id}`)
    try {
        const orders= await service.update( req.params.id, req.body, req.context)
        log.end()
        return response.data(res,orders)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
