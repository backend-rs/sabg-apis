'use strict'

const response = require('../exchange/response')
const service = require('../services/slider')
const mapper = require('../mappers/slider')


exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/slider`)

    try {
        const slider= await service.create(req.body, req.context)
        log.end()
        return response.data(res, slider)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/slider/get/${req.params.id}`)
    try {
        const slider = await service.getById(req.params.id, req.context)
        return response.data(res, slider)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/slider/get`)
    try {
        const slider = await service.get(req,req.context)
        log.end()
        return response.data(res, slider)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.update = async (req, res) => {
    const log = req.context.logger.start(`api/slider/update${req.params.id}`)
    try {
        const slider = await service.update(req, req.params.id, req.body, req.context)
        log.end()
        return response.data(res, slider)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}


// exports.delete = async (req, res) => {
//     const log = req.context.logger.start(`api/categories/delete${req.params.id}`)
//     try {
//         const category = await service.deletecategory(req.params.id, req.context)
//         log.end()
//         return response.data(res, 'successfully delete category')
//     } catch (err) {
//         log.error(err.message)
//         log.end()
//         return response.failure(res, err.message)
//     }
// }
// exports.delete = async (req, res) => {
//     try {
//         const category = await service.delete(req)
//         return response.data(res, category)
//     } catch (err) {
//         return response.failure(res, err.message)
//     }
// }