'use strict'

const { loginType } = require("../models/user")

// // users create mapper
exports.toModel = entity => {
    var model = {
        _id: entity._id,
        cartId: entity.cartId,
        isVerified: entity.isVerified,
        type: entity.type,
        // name: entity.name,
        phone: entity.phone,
        email:entity.email,
        userName:entity.userName,
        // otp:entity.otp,
        expiryTime: entity.expiryTime,
        token: entity.token,
        
         loginType:entity.loginType
    }

    if (entity.deliveryAddress) {
        model.deliveryAddress = {
            line1: entity.deliveryAddress.line1,
            line2: entity.deliveryAddress.line2,
            city: entity.deliveryAddress.city,
            district: entity.deliveryAddress.district,
            state: entity.deliveryAddress.state,
            pinCode: entity.deliveryAddress.pinCode,
            country: entity.deliveryAddress.country,
        }
    }
    return model
}

// for verifyRegOtp
exports.toVerifyOtp = entity => {
    var model = {
        _id: entity._id,
        isVerified: entity.isVerified,
        type: entity.type,
        // name: entity.name,
        phone: entity.phone,
        token: entity.token
    }
    return model
}

// for particular user
exports.toGetUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        isVerified: entity.isVerified,
        name: entity.name,
        email: entity.email,
        phone: entity.phone,
        email:entity.email,
        userName:entity.userName,
        loginType:entity.loginType
    }
    if (entity.address) {
        model.address = {
            line1: entity.address.line1,
            line2: entity.address.line2,
            city: entity.address.city,
            district: entity.address.district,
            state: entity.address.state,
            pinCode: entity.address.pinCode,
            country: entity.address.country,

        }
    }


    return model
}

// for login 
exports.toUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        isVerified: entity.isVerified,
        name: entity.name,
        phone: entity.phone,
        token: entity.token,
        cartId: entity.cart._id,
        loginType:entity.loginType,
        email:entity.email,
        userName:entity.userName,
    }
    return model
}
exports.toSendToken = entity => {
    return {
        tempToken: entity.tempToken
    }
}
